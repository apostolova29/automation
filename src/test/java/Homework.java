import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.plaf.PanelUI;

public class Homework {
    WebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "C:\\Верстка\\geckodriver.exe");
    }

    @Before
    public void before() {
        driver = new FirefoxDriver();
    }


    @Test
    public void openUserData() {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.xpath("//input[@value='Registration']")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys("Nastya", Keys.ENTER);
        driver.findElement(By.cssSelector("#last_name")).sendKeys("Apostolova", Keys.ENTER);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys("7772255", Keys.ENTER);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys("380935298423", Keys.ENTER);
        driver.findElement(By.cssSelector("#field_email")).sendKeys("nastya15@ukr.net", Keys.ENTER);
        driver.findElement(By.cssSelector("#field_password")).sendKeys("Nastya1515", Keys.ENTER);
        driver.findElement(By.xpath("//input[@name='gender' and @value='female']")).click();
        Select position = new Select(driver.findElement(By.id("position")));
        position.selectByVisibleText("qa");
        position.selectByValue("qa");
        driver.findElement(By.cssSelector("#button_account")).click();
        Alert alert = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();


    }
}